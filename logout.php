<?php session_start();
require_once 'includes/connection.php';
require_once 'includes/functions.php';

    if(isset($_SESSION['name'])){
        session_unset($_SESSION);
        session_destroy($_SESSION);
        redirect_to('index.php');
    }else{
        session_destroy($_SESSION);
        redirect_to('index.php');
    }
  ?>