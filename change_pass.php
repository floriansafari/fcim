<?php 
    session_start();
    require_once 'includes/connection.php';
    require_once 'includes/functions.php';
   if(!isset($_SESSION['name'])){
    redirect_to('index.php');
   }
?>
<html>
<head><title>FCIM-Change password</title></head>
<body>
<link href="includes/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<div class = "main col-sm-offset-1 col-sm-10">
    <!--leaves a column before it begins and leaves another space at the end-->
   <?php head();?>
    <div class = "main_content" style ="height:100%"><!-- for login form -->
        <div class = "login_form col-sm-offset-1 col-sm-7"><!-- put some gradients here -->
            <div class = "center-block ">
                
                <?php menu_line1(); ?>
              
                <h2 style = "text-align:left">Pasword changing</h2><br>
                 <?php 
                 $status = isset($_GET['status']) ? $_GET['status']: "";
                 if($status == ""){//form has been submited
                 $changed = isset($_GET['changed']) ? $_GET['changed']: "";
                        if($changed == 'yes'){
                            echo "<div class = 'alert alert-success col-sm-12'>
                                You Password was changed.
                            </div>";
                        }elseif($changed == 'no_match_new_old'){
                                echo "<div class = 'alert alert-warning col-sm-12'>
                                New password and confirmed password must match.
                            </div>";
                        }elseif($changed == 'wrong_old_pass'){
                            echo "<div class = 'alert alert-danger col-sm-12'>
                                Wrong old password.
                            </div>";
                        }
                                $query = "SELECT * from users";
                                $result  =mysql_query($query);
                                $name = $_SESSION['name'];
                                $pass_change_atempts = 0;
                                $admin =0;
                                $end=0;
                                if(!isset($result)){
                                    echo "No users registred!<br />";
                                }
                                elseif(isset($result)){
                                    while($row  = mysql_fetch_array($result)){
                                        if($row['uname'] == $name){
                                                $pass_change_atempts = $row['pass_change_count'];
                                              if($row['prev'] ==3){
                                                    $admin+=1;
                                              }
                                        }
                                    }
                               }
                              if($admin==1){
                                $pass_change_left =0;
                                    if($pass_change_atempts>=8 && $pass_change_atempts<10){//password change reached critical point
                                         $pass_change_left = 10  - $pass_change_atempts;
                                        echo "<div class = 'alert alert-warning col-sm-12'>
                                        You can only change your password three times.<br />
                                        You have only  &nbsp(".$pass_change_left."&nbsp attempt(s) left)
                                        </div>";
                                    }
                                    elseif($pass_change_atempts >0 && $pass_change_atempts<8){//password change not reached limit
                                        $pass_change_left = 10  - $pass_change_atempts;
                                       echo "<div class = 'alert alert-info col-sm-12'>
                                         You can only change your password three times. &nbsp(".$pass_change_left."&nbsp attempts left)
                                         </div>";
                                    }elseif($pass_change_atempts >10){//password change reached limit. cant be changed.
                                       $end +=1;
                                        echo "<div class = 'alert alert-danger col-sm-12'>
                                         You reached password change limit. You can't change your password anymore.
                                    </div>";
                                        }elseif($pass_change_atempts ==9){//password change reached limit. cant be changed.
                                            
                                        echo "<div class = 'alert alert-warning col-sm-12'>
                                         This is your last time to change your password.
                                    </div>";
                                        } 
                                }//end of admin pass change regualtions
                              //beggin normal user monitoring
                               elseif($admin==0){
                                   if($pass_change_atempts <3){//password change not reached limit
                                       if($pass_change_atempts == 0){
                                            echo "<div class = 'alert alert-info col-sm-12'>
                                                 You can only change your password three times.(3 attempts left)
                                            </div>";
                                       }elseif($pass_change_atempts==1){
                                            echo "<div class = 'alert alert-info col-sm-12'>
                                                 You can only change your password three times.(2 attempts left)
                                            </div>";
                                       }elseif($pass_change_atempts==2){
                                        $end +=2;
                                                echo "<div class = 'alert alert-danger col-sm-12'>
                                                 You can only change your password three times.<br />
                                                 This is your last change to change your password
                                                </div>";
                                       }
                                    }
                                }//end of determinng normal users
                if($end!=1 || $end==2){?>
                <form action = "pass_change.php" method = "post">
                 Old password:
                    <input required type = "password" class = "form-control" name = "old_password" placeholder = "Old password">
                    <br> 
                    
                    New Password:
                    <input required  type = "password" class = "form-control" name = "new_password" placeholder = "New Password"><br>
                    
                    Confirm Password:
                    <input required  type = "password" class = "form-control" name = "confirm_password" placeholder = "Confirm Password"><br>

                    <input  class="mybutton col-sm-6" type = "submit" name = "submit" value = "Change">
                </form>
                <br><br><br><br ><!-- show how many attempls left-->
               <?php }
               
        }else{
            echo "<div class = 'alert alert-danger '>";
                    echo "Sorry! you have erached password changing limit. You cant change the password anymore.";
              echo "</div><br>";
        }
               ?>
                
            </div>
          </div>
     </div>
</div>
<?php footer();?>
</body>
</html>