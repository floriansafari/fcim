<?php 
    //index page for login and registering option
    session_start();
    require_once 'includes/connection.php';
    require_once 'includes/functions.php';
    
    $course = trim(mysql_prep($_POST['course']));
   //get all users
    $query = "SELECT * from users";
    $result = mysql_query($query);
    $qualified = 0;
    $course_taking ="";
    $name = $_SESSION['name'];
    if(!isset($result)){//No results returned
        echo "There was an error in getting users. Please inform administrator.";
    }
    elseif(isset($result)){//there are users
        while($row = mysql_fetch_array($result)){
            if($name==$row['uname']){//username is available in database
                //see if user has reqired skills to apply
                if($row['edu_level']>=6){//user has finished form six
                    if($row['no_of_pass']>=2){//number of passes
                    //user has more than or two passes. 
                        if(!isset($row['course_taking'])){  //user is not enroled to any course
                            $qualified+=1;
                        }elseif(isset($row['course_taking'])){//user is already enroled
                            $qualified+=2;
                            $course_taking = $row['course_taking'];
                        }
                    }//user has no more than or two passes, cant enrol.
                }//user has not finished form six, cant enrol.
            }//username not available in database,cant enrol.
        }
        if($qualified==0){//user has no qualifications 
           //user has not qualified for course enrolment
            redirect_to("courses.php?enroled=not_qualified&course=$course");
        }elseif($qualified==2){//user has already enroled in one course
            redirect_to("courses.php?enroled=already_enroled&course=$course_taking");
        }elseif($qualified==1){//user has all qualifications for course enrolment. 
            //check if user is withing enroling period, before updating
            $query = "SELECT * from courses where course_name = '$course'";//get courses
            $result = mysql_query($query);
            $found=0;
            $late =0;
            $date = date("Y-m-d");
                if(!isset($result)){
                    echo "There was a problem in database. No results returned. &nbsp";
                    echo mysql_error();
                }elseif(isset($result)){ //there are courses in database
                    while($row = mysql_fetch_array($result)){
                        $found+=1;
                        if($date >= $row['enrol_begin'] && $date <= $row['enrol_end']){ 
                            //user is within enrolment period
                                $late+=1;
                        }elseif($date < $row['enrol_begin']){
                            //user want to enrol in a course that has not started enroling students 
                            $date = $row['enrol_begin'];
                            $late+=2;
                        }elseif($date > $row['enrol_end']){
                            //user is late in enroling to the course
                            $date = $row['enrol_end'];                        
                            $late+=3;
                        }
                    }
             if($found==0){
                    echo "Error! There are no courses. Pleasse contact your administrator.";
                    exit();
                }
             if($late==1){//user is within enrolment period. continue with updating queriws
                $query = "UPDATE courses set students = students + 1 where course_name = '$course'";
                $result = mysql_query($query);
                if(!isset($result)){//there are no results from query
                    echo "There is a problem in updating number of students.&nbsp";
                    echo mysql_error();
                }elseif(isset($result)){//there were no problem. students number update successfull
                        //update course name to user's table
                        $query = "UPDATE users set course_taking = '$course' where uname = '$name'";
                        $result = mysql_query($query);
                        if(!isset($result)){//there are no results from query
                            echo "There is a problem in inserting course name to users table.&nbsp";
                            echo mysql_error();
                        }elseif(isset($result)){//there were no problem.insertion successfull
                            //set global variable to detect user has already enroled
                             $_SESSION['enroled']=array();
                             $_SESSION['enroled'] ="yes";
                            //redirect to home
                            redirect_to("courses.php?enroled=yes&course=$course");
                        }
                    //update complete
                }
            }elseif($late==2){//user want to enrol in a course that has not started enroling students
                redirect_to("courses.php?enroled=early&course=$course&date=$date");
            }elseif($late==3){
                //user is late in enroling to the course
                redirect_to("courses.php?enroled=late&course=$course&date=$date");
            }
        }
        //end of qualified users
        }
    }

?>