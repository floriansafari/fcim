<?php 
 session_start();
require_once 'includes/connection.php';
require_once 'includes/functions.php';
if(isset($_SESSION['name'])){//user has loged in
                ?>
                <html>
                <head><title>FCIM-Home</title></head>
                <body>
                <link href="includes/css/bootstrap.min.css" rel="stylesheet">
                <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
                <div class = "main col-sm-offset-1 col-sm-10">
                    <!--leaves a column before it begins and leaves another space at the end-->
                    <?php head();?>
                    <div class = "main_content"  style ="height:100%"><!-- for login form -->
                        <div class = "login_form col-sm-offset-1 col-sm-8"><!-- put some gradients here -->

                            <h2>Welcome <?php echo strtoupper($_SESSION['name']) ?></h2><br>
                            <div class = "center-block " >
                                <?php menu_line1();?>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
    <?php 
        }elseif(!isset($_SESSION['name'])){//user has not loged in 
            redirect_to('index.php');
        }
    ?>
    <?php footer();?>
</body>
</html>