<?php 
    session_start();
    require_once 'includes/connection.php';
    require_once 'includes/functions.php';
   if(!isset($_SESSION['name'])){
    redirect_to('index.php');
   }
?>
<html>
<head><title>FCIM-Courses</title>
<link href="includes/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="includes/js/bootstrap.js"></script>
</head>
<body>

<div class = "main col-sm-offset-1 col-sm-10">
    <!--leaves a column before it begins and leaves another space at the end-->
    <?php head();?>
    <div class = "main_content" style ="height:100%"><!-- for login form -->
        <div class = "login_form col-sm-offset-1 col-sm-7"><!-- put some gradients here -->
            <div class = "center-block ">
             <?php menu_line1(); ?>
                <h2 style = "text-align:left">
                    FCIM COURSES
                </h2><br>
                      <?php 
                    $enroled = isset($_GET['enroled']) ? $_GET['enroled']: "";
                    $course = isset($_GET['course']) ? $_GET['course']: "";
                    $date = isset($_GET['date']) ? $_GET['date']: "";
                    if(!isset($_SESSION['enroled'])){
                        $_SESSION['enroled']=array();
                        $_SESSION['enroled'] ="";
                    }
                            if($enroled == 'yes' && $course !=""){
                            ?>
                            <div class = "alert alert-success ">
                                You have been enroled in 
                                <?php echo strtoupper($course) ?> course.
                             </div><br>
                            <?php }
                            elseif($enroled=='not_qualified'){
                                ?>
                            <div class = "alert alert-warning ">
                                Sorry! You have no qualifications to be enroled in 
                                <?php echo strtoupper($course) ?> course.
                             </div><br>
                            <?php
                            }elseif($enroled=='already_enroled'){
                                ?>
                                 <div class = "alert alert-danger">
                                    Sorry! You are already enroled in <?php echo strtoupper($course)  ?> course.<br>
                             </div><br>
                            <?php }elseif($enroled=='early'){
                               ?>
                                 <div class = "alert alert-info">
                                    Sorry! This course have not started enroling students.<br>
                                    Enrolment will start on: <?php echo $date;?><br />
                             </div><br>
                            <?php
                            }elseif($enroled=='late'){
                                    ?>
                                 <div class = "alert alert-danger">
                                    Sorry! Enrolment for this course ended on:<?php echo $date;?>.<br>
                             </div><br>
                            <?php
                            }
                            elseif($enroled=="" && $course == "" ){
                 //nothng returned. give the choices
                 echo "<h3> Do you want to enroll? </h3><br>";
                echo "<h5 class = 'info'>
                Select a course you wish, and click 'APPLY' to enrol.<br/>
                Note:  You can only apply for one course</h5>";
                
                $query = "SELECT * from courses";//get courses
                $result = mysql_query($query);
                $found =0;
                $name = $_SESSION['name'];
                if(isset($result)){
                    //there are courses from database
                            echo "<div class = 'courses'>";
                                
                                echo "<form action = 'enrol.php' method = 'post'>";
                                    echo "<select required name = 'course'>";

                                        echo "<option value = ''>NONE</option>";
                                        while($row = mysql_fetch_array($result)){
                                          $found+=1;
                                          echo "<option value =".$row['course_name'].">".
                                              strtoupper($row['course_name'])."&nbsp(".
                                              strtoupper($row['course_description']).
                                          ")</option>";
                                        }
                                     echo "</select>";
                                     echo '<br><br><input type = "submit" class = "mybutton" value = "APPLY">';
                                 echo "</form>";
                            echo "</div>";
                }elseif(!isset($result)){
                    echo "There was a problem in database. No results returned. &nbsp";
                    echo mysql_error();
                }
                if($found==0){
                    echo "Error! There are no courses. Pleasse contact your administrator.";
                }
            }
          ?>
            </div>
          </div>
     </div>
</div>
<?php footer();?>
</body>
</html>