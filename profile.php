<?php 
    session_start();
    require_once 'includes/connection.php';
    require_once 'includes/functions.php';
   if(!isset($_SESSION['name'])){
    redirect_to('index.php');
   }
?>
<html>
<head><title>FCIM-Profile</title>
<link href="includes/css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="includes/js/bootstrap.js"></script>
</head>
<body>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<div class = "main col-sm-offset-1 col-sm-10">
    <!--leaves a column before it begins and leaves another space at the end-->
   <?php head();?>
    <div class = "main_content"  style ="height:100%"><!-- for login form -->
        <div class = "login_form col-sm-offset-1 col-sm-7"><!-- put some gradients here -->
            <div class = "center-block ">
                <?php menu_line1();?>
                <h2 style = "text-align:left">
                    <?php echo strtoupper($_SESSION['name']);?>'S PROFILE
                </h2><br>
                <?php 
                $query = "SELECT * from users";
                $result = mysql_query($query);
                    $found =0;
                    $name = $_SESSION['name'];
                if(isset($result)){
                    //there are results from database
                    while($row = mysql_fetch_array($result)){
                        if($row['uname'] == $name){
                            $found+=1;
                            $fname = $row['fname'];
                            $sname = $row['sname'];
                            $sex = $row['gender'];
                            $email = $row['email'];
                            $dob = $row['dob'];
                            $image_name =  $row['id'];
                        }
                    }
                }elseif(!isset($result)){
                    echo "There was a problem in database.".mysql_error();
                }
                if($found==1){
                    $directory = "images";
                     echo "<div class = 'col-sm-6'>";
                    echo "<img src='".get_available_image($image_name, $directory)."' /> 
                    </div>";
                   
                echo "<div class = col-sm-8>";
                echo "<label class = 'info'>First Name: &nbsp".strtoupper($fname)."</label> <br>";
                echo "<label  class = 'info'>Surname: &nbsp ".strtoupper($sname)."</label> <br>";
                echo "<label  class = 'info'>Date of birth (yyyy-mm-dd): &nbsp".strtoupper($dob)."</label> <br>";
                echo "<label  class = 'info'>Gender: &nbsp".strtoupper($sex) ."</label> <br>";
                echo "<label  class = 'info'>E-mail:  &nbsp $email</label> <br><br>";
                echo "<a href='edit_profile.php' class = 'mybutton1'> Edit Details</a>";
                echo "</div>";

                }elseif($found==0){
                    echo "Error in finding your profile. Pleasse contact your administrator.";
                }
                ?>
            </div>
          </div>
     </div>
</div>
<?php footer();?>
</body>
</html>