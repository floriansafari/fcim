<?php
function redirect_to($location = NULL){
  if($location != NULL){
  header("Location: {$location}");
      exit;
    }
}
/**
* Searches for the correct extension of the image in a folder and returns the first instance of the available image with the name<br>
* Search order and allowed extensions: png, jpg, jpeg, gif
*
* @param $image_name the filename to search the extension for
* @param $directory The folder path to search for the image
* @return String containing the first instance of the image file with the name or FALSE if no valid SAS image was found
*/
function get_available_image($image_name, $directory)
{

  $allowed_extensions = array('png', 'jpg', 'jpeg', 'gif');

  //detect whether the directory path entered doesnt have a '/' at the end and append it
  if (strrchr($directory, '/') != '/')
    $directory .= '/';

  foreach ($allowed_extensions as $ext)
  {
    $file_str = $directory . $image_name . '.' . $ext;
    if (file_exists($file_str))
    {
      $ret = $file_str;
      break;
    }
  }
  if (isset($ret))
    return $ret;
  else
    return FALSE;

}
function head(){?>
  <div class = "header"><!--for top blue bar for logo on the left side, and heading at the center -->
                         <div class = "logo"><!--for IFM logo -->
                        <img src ='images/img1.png' class = " img-thumbnail" style = " height: 150px; width: 150px">
                        </div>
                        <div class = "header_top_right"><!-- for center heading-->
                             FACULTY OF COMPUTING INFORMATION SYSTEMS AND MATHEMATICS (FCIM)
                        </div>
                    </div>
<?php }
function footer(){
  ?>
  <div><hr> </div>
<?php 
}
function menu_line(){
        echo '<div class = "center-block " >
           <ul class="nav nav-pills">  
             <li class="">
                <a href="">Home</a>
            </li>
            <li class="">
                <a href="profile.php">View profile</a>
            </li> 
            <li>
                <a href="change_pass.php">Change password</a>
            </li>
             <li>
                <a href="students.php">FCIM Students</a>
            </li> 
            <li>
                <a href="logout.php">Logout&nbsp('.strtoupper($_SESSION['name']).')</a>
            </li> 
          </ul>
         </div>';
}
function menu_line1(){
              echo ' <ul class="nav nav-pills">  
                 <li class="">';
                 if($_SESSION['prev']==3){
                  echo "<a href='admin.php'>Home</a>";
                 }else{
                  echo "<a href='home.php'>Home</a>"; 
                 }
                echo '</li>
                <li class="">
                    <a href="profile.php">View profile</a>
                </li> 
                <li>
                    <a href="change_pass.php">Change password</a>
                </li>';
                if($_SESSION['prev']==1){
                  echo '<li>
                    <a href="courses.php">FCIM courses</a>
                </li> ';
                }elseif($_SESSION['prev']==3){
                  echo '<li>
                    <a href="students.php">FCIM Students</a>
                </li>';
                }
                 echo '<li>
                    <a href="logout.php">Logout&nbsp('.strtoupper($_SESSION['name']).')</a>
                </li> 
              </ul>';
}
function mysql_prep($value){
    $magic_quotes_active = get_magic_quotes_gpc();
    $new_enough_php = function_exists("mysql_real_escape_string"); //i.e for mysql after 4.3.0
    if($new_enough_php){//php available is later than or equal to 4.3.0
      //so undo magic quotes effects so that mysql_real_escape_string can work
      if($magic_quotes_active){ $value =stripslashes($value); }
      $value = mysql_real_escape_string($value);
      }else {//mysql before 4.3.0
        //if magic quotes aren't already on then set them mannualy
        if(!$magic_quotes_active){
          $value = addslashes($value);}
          //if magic quotes are active, then the slashes already exists
      }
      return $value;
   }
?>