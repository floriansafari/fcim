<?php 
    session_start();
    require_once 'includes/connection.php';
    require_once 'includes/functions.php';
   if(!isset($_SESSION['name'])){
    redirect_to('index.php');
   }
?>
<html>
<head><title>FCIM-Edit Profile</title>
<link href="includes/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>
<div class = "main col-sm-offset-1 col-sm-10">
    <!--leaves a column before it begins and leaves another space at the end-->
    <?php head();?>
    <div class = "main_content"  style ="height:100%"><!-- for login form -->
        <div class = "login_form col-sm-offset-1 col-sm-7"><!-- put some gradients here -->
            <div class = "center-block ">
               
               <?php menu_line1(); ?>

                <h2 style = "text-align:left">
                    <?php echo strtoupper($_SESSION['name']);?>'S PROFILE
                </h2><br>
                    <?php 
                      $changes = isset($_GET['changes']) ? $_GET['changes']: "";
                      if($changes == 'commited'){
                        echo "<div class = 'alert alert-info'>";
                        echo"Changes successfuly made.
                             </div><br>";
                      }
                    ?>
                <h3> Edit Profile</h3>
                <?php 
                $query = "SELECT * from users";
                $result = mysql_query($query);
                    $found =0;
                    $name = $_SESSION['name'];
                if(isset($result)){
                    //there are results from database
                    while($row = mysql_fetch_array($result)){
                        if($row['uname'] == $name){
                            $found+=1;
                            $fname = $row['fname'];
                            $sname = $row['sname'];
                            $sex = $row['gender'];
                            $email = $row['email'];
                            $dob = $row['dob'];
                           $image_name =  $row['id'];

                        }
                    }
                }elseif(!isset($result)){
                    echo "There was a problem in database.".mysql_error();
                }
                if($found==1){ ?>
                    <div class = 'col-sm-8'>
                        <form method = "post" action = "commit_changes.php" enctype="multipart/form-data">
                         <label>First Name :</label>
                        <input required type = "text" class = "form-control" name = "fname" placeholder = "First Name" value = "<?php echo strtoupper($fname)?>" >
                        <br> 

                        <label>SurName:</label>
                        <input required type = "text" class = "form-control" name = "sname" placeholder = "Surname"  value = "<?php echo strtoupper($sname)?>" >
                        <br> 
                     <label> Gender:</label>
                         <select required name ="gender">
                            <?php if($sex == "male" ){
                                echo "<option selected value = 'male'>MALE</option>";
                                echo "<option value = 'female'>FEMALE</option>";
                            }elseif($sex == "female"){
                                 echo "<option value = 'male'>MALE</option>";
                                echo "<option selected value = 'female'>FEMALE</option>";
                            }
                            ?>
                         </select>
                         <br><br>
                       
                       <label> Date of birth (month/date/year):</label>
                         <input required type = "Date" name = "dob" value ="<?php echo $dob ?>" ><br><br>
                       
                       <label>Email:</label>
                        <input required type = "text" class = "form-control" name = "email" placeholder = "E-mail" value = "<?php echo $email ?>">
                        <br>
                        <input  class="mybutton col-sm-6" type = "submit" name = "submit" value = "SUBMIT CHANGES">
                        
                    </div>
                     <?php //image
                     $directory = "images";

                           echo "<div class = 'col-sm-4'>
                           <img src='".get_available_image($image_name, $directory)."' />";
                           echo "<input type = 'file' name='passport' value = 'Upload passport image'>
                         </div>";?>

                </form>
                <?php }elseif($found==0){
                    echo "Error in finding your profile. Pleasse contact your administrator.";
                }
                ?>
            </div>
          </div>
     </div>
</div>
<?php footer();?>
</body>
</html>