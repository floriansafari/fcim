-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2016 at 01:15 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fcim`
--
CREATE DATABASE IF NOT EXISTS `fcim` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `fcim`;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `course_name` varchar(40) NOT NULL,
  `course_description` varchar(80) DEFAULT NULL,
  `course_duration` int(11) NOT NULL,
  `course_fees` int(11) NOT NULL,
  `enrol_begin` date DEFAULT NULL,
  `enrol_end` date DEFAULT NULL,
  `students` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`course_name`, `course_description`, `course_duration`, `course_fees`, `enrol_begin`, `enrol_end`, `students`) VALUES
('bcs', 'Bachelor in Computer Science', 3, 1750000, '2016-02-01', '2016-03-01', 1),
('bit', 'Bachelor in Information Technology', 3, 1050000, '2016-01-01', '2016-02-19', 0),
('odcs', 'Ordinary Diploma in Computer Science', 2, 900000, '2016-01-01', '2016-03-01', 0),
('odit', 'Ordinary Diploma in Information Technology', 2, 850000, '2015-12-31', '2016-04-01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `sname` varchar(40) NOT NULL,
  `fname` varchar(40) NOT NULL,
  `gender` varchar(7) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(40) NOT NULL,
  `img` varchar(100) DEFAULT NULL,
  `uname` varchar(50) DEFAULT NULL,
  `hashed_password` varchar(50) DEFAULT NULL,
  `pass_change_count` int(11) NOT NULL DEFAULT '0',
  `prev` int(11) DEFAULT NULL,
  `edu_level` int(11) NOT NULL DEFAULT '0',
  `course_taking` varchar(50) DEFAULT NULL,
  `no_of_pass` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `sname`, `fname`, `gender`, `dob`, `email`, `img`, `uname`, `hashed_password`, `pass_change_count`, `prev`, `edu_level`, `course_taking`, `no_of_pass`) VALUES
(33, 'MASHY', 'MASHY', 'female', '1994-09-14', 'happymashy@gmail.com', NULL, 'mashy', 'bcc105631d234cbb723dd372f6dc0feda2352d78', 1, 1, 7, 'bcs', 3),
(34, 'admin', 'admin', 'male', '1990-10-18', 'admin@ymail.com', NULL, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1, 3, 7, NULL, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`course_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
