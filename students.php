<?php 
 session_start();
require_once 'includes/connection.php';
require_once 'includes/functions.php';
    if(isset($_SESSION['name'])){//user has loged in
        if($_SESSION['prev'] !=3){
            redirect_to('logout.php');
        }elseif($_SESSION['prev'] ==3){//user is admin, proceed.
            echo "<html>
            <head>
                <title>FCIM-Students</title>
            </head>
            <body>" ?>
                <link href="includes/css/bootstrap.min.css" rel="stylesheet">
                <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
                <div class = "main col-sm-offset-1 col-sm-10">
                    <!--leaves a column before it begins and leaves another space at the end-->
                 <?php head();?>
                    <div class = "main_content"  style ="height:100%"><!-- for login form -->
                        <div class = "login_form col-sm-offset-1 col-sm-8"><!-- put some gradients here -->
                            <?php menu_line(); 
                            echo "<h3 class = 'info'> Students search</h3>";
                            echo "<h4> Search students by course: </h4>";
                            $query = "SELECT * from courses";//get courses
                                    $result = mysql_query($query);
                                    $found =0;
                                    if(!isset($result)){//no results from query
                                        echo "There was a problem in database. No courses results returned. &nbsp";
                                        echo mysql_error();
                                    }else{//there were results from query
                        ?>              <form action = 'students.php' method = 'get'>
                                            <select required name= 'course'>
                                                <option value = ''> Select course</option>
                                                <?php 
                                                        while($row = mysql_fetch_array($result)){
                                                            $found+=1;
                                                            echo "<option value =".$row['course_name'].">
                                                            ". strtoupper($row['course_name'])."&nbsp(
                                                            ".strtoupper($row['course_description']).
                                                            ")</option>";
                                                        }
                                                 ?>   
                                            </select><br><br>
                                            <input class = 'mybutton1' type = "submit" value = 'SEARCH'><br><br>
                                        </form>
                                    <?php }// END OF GETTING COURSES

                                   
                                    //now get students who are enroled to that course
                                    
                                    $course  = isset($_GET['course']) ? $_GET['course']: "";
                                    if($course !=""){//there is a course returned
                                        $query = "SELECT * from users where course_taking = '$course'";//get courses
                                        $result = mysql_query($query);
                                        $found =0;
                                        if(isset($result)){
                                           //there is data, crete table and show them there
                                            echo "<table class = 'table table-hover  table-bordered'>
                                                   <thead>
                                                        <th>First name</th>
                                                        <th>Surname</th>
                                                        <th>Gender</th>
                                                        <th>E-mail</th>
                                                        <th>Course</th>
                                                    </thead>
                                                    <tbody>";
                                                while($row = mysql_fetch_array($result)){
                                                echo "<tr>";
                                                    echo "<td>";
                                                        echo strtoupper($row['fname']);
                                                    echo "</td>";
                                                    echo "<td>";
                                                        echo strtoupper($row['sname']);
                                                    echo "</td>";
                                                    echo "<td>";
                                                        echo strtoupper($row['gender']);
                                                    echo "</td>";
                                                    echo "<td>";
                                                        echo $row['email'];
                                                    echo "</td>";
                                                    echo "<td>";
                                                        echo strtoupper($row['course_taking']);
                                                    echo "</td>";
                                                echo "</tr>";
                                                   }
                                            //end table
                                            echo "</tbody>";
                                            echo "</table>";
                                               }else{
                                                echo "No students in the course";
                                               }
                                         }//end of course
                                    }
                                    ?>
                        </div>
                    </div>
                </div>
    <?php 
            echo "</body>
            </html>";   
        }else{//user has not loged in
        redirect_to('index.php');
    }       
?>