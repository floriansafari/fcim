# README #

This is a college assignment, requiring me to create a web based platform for The Faculty of Computing Information Systems and Mathematics.
### What is this repository for? ###

* Quick summary
* Version: 1

### How do I get set up? ###

* Summary of set up
After cloning the repo, import the fcim.sql file to your server

* Configuration
After cloning/downloading the repo, open your browser (Chrome is advised) and type in the server's IP Address followed by "/fcim/index.php".
If every thing is well, you should see a login page, where the username is: admin and lassword is "pass123"
Now you are good to go

* Database configuration
Open constants.php file in includes folder and set the database constants that apply to your server



### Who do I talk to? ###
* Repo owner or admin: Florian Safari
* Contacts: floriansafari@gmail.com