<?php 
    //index page for login and registering option
    session_start();
    require_once 'includes/connection.php';
   require_once 'includes/functions.php';
   if(isset($_SESSION['name'])){
        redirect_to('home.php');
    }
?>
<html>
<head><title>FCIM-Register</title></head>
<body>
<link href="includes/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<div class = "main col-sm-offset-1 col-sm-10">
    <!--leaves a column before it begins and leaves another space at the end-->
<?php head();?>
    <div class = "main_content" style ="height:100%"><!-- for login form -->
        <div class = "login_form col-sm-offset-2 col-sm-7"><!-- put some gradients here -->
                <form action = "register.php" method = "post" enctype="multipart/form-data">
                    <div class = "center-block ">
                        <h2>Registration Form</h2><br>
                        <a href="index.php" class = "link">I have account</a><br><br>
                        <?php 
                        $status = isset($_GET['status']) ? $_GET['status']: "";
                        if($status == 'not_submited'){
                                ?>
                            <div class = "alert alert-danger ">
                                Form was not submited properly.
                             </div><br>
                            <?php
                            }
                        ?>
                       <h3> User Credentials</h3><br />
                       <label>Surname:</label>
                        <input required type = "text" class = "form-control" name = "sname" placeholder = "Surname">
                        <br> 
                        
                        <label>First Name:</label>
                        <input required type = "text" class = "form-control" name = "fname" placeholder = "First Name">
                        <br> 
                       
                       <label>Gender:</label>
                         <input required value = "male"name="sex"type = "radio">Male
                         <input required value = "female" name="sex"type = "radio">Female<br><br>
                       
                       <label> Date of birth (month/date/year):</label>
                         <input required type = "Date" name = "dob"><br><br>
                       
                       <label>Email:</label>
                        <input required type = "text" class = "form-control" name = "email" placeholder = "E-mail">
                        <br>
                         <!-- look for way to select image from coputer
                           <label>Select passport photo</label>
                            <input type = >
                        -->
                        <h3> Academic Qualifications</h3>
                        <label> Highest Education Level:</label>
                        <select required name  = "edu_level">
                          <option value = ''> ----------------</option>
                          <option value = 'std_seven'> Standart Seven</option>
                          <option value = 'four'> Form Four</option>
                          <option value = 'five'> Form Five</option>
                          <option value = 'six'> Form Six</option>
                          <option value = 'bachelor'> Bachelor</option>
                        </select>
                        <br><br>
                       <label>Number of pass: </label>
                        <select required name  = "no_of_pass">
                          <option value = ''> ---</option>
                          <option value = '1'>1</option>
                          <option value = '2'> 2</option>
                          <option value = '3'> 3</option>
                         </select><br>
                         <label class = "info">(Pass include A, B, C and D ) </label>
                        <div >
                          <input type = "file" name="passport" value = "Upload passport image">
                          <br>
                         <input  class="mybutton col-sm-6 " type = "submit" name = "submit" value = "Register">
                        </div>
                  </div>
                </form>
        </div>
      <br>
</div>
<?php footer();?>
</body>
</html>