<?php 
    //index page for login and registering option
    session_start();
    require_once 'includes/connection.php';
     require_once 'includes/functions.php';
?>
<html>
<head><title>FCIM-Login</title></head>
<body>
<link href="includes/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<div class = "main col-sm-offset-1 col-sm-10">
    <!--leaves a column before it begins and leaves another space at the end-->
    <?php head();?>
    <div class = "main_content"  style ="height:100%"><!-- for login form -->
        <div class = "login_form col-sm-offset-2 col-sm-5"><!-- put some gradients here -->
                <form action = "auth.php" method = "post">
                    <div class = "center-block ">
                        <h2 style = "text-align:left">Welcome please login</h2><br>
                        <?php 
                        $status = isset($_GET['status']) ? $_GET['status']: "";
                            if($status == 'not_found'){
                                ?>
                            <div class = "alert alert-danger ">
                                Username and Password match not found.
                             </div><br>
                            <?php
                            }elseif($status == 'not_submited'){
                                ?>
                            <div class = "alert alert-danger ">
                                Form was not submited properly.
                             </div><br>
                            <?php
                            }
                        ?>
                        <div class ="glyphicon glyphicon-user"></div>
                        User Name
                        <input required type = "text" class = "form-control" name = "uname" placeholder = "Username">
                        <br>  
                        <div class ="glyphicon glyphicon-lock"></div>
                        Password
                        <input required  type = "password" class = "form-control" name = "password" placeholder = "Password"><br>
                        <a href="reg.php" class = "link">Register me</a><br><br>
                        <input  class="mybutton col-sm-6" type = "submit" name = "submit" value = "Login">
                    </div>
             </form>
            <br><br><br><br >
        <div class = "alert alert-info col-sm-12">
            FOR FIRST TIME USERS:
            <p>Use your <b>SURNAME</b> as username and <br />
             your <b>DATE OF BIRTH (yyyy-mm-dd)</b> as your password. </p>
            
        </div>

</div></div></div>
<?php footer();?>
</body>
</html>